chicken-syslog-port
===================
Write to syslog as an output port.

Requirements
------------
 - [module-declarations](http://api.call-cc.org/doc/module-declarations)
 - [syslog](http://api.call-cc.org/doc/syslog)

Installation
------------
    $ chicken-install syslog-port

Documentation
-------------
Documentation is available on [chickadee][] and the [CHICKEN
wiki][wiki].

[wiki]: http://wiki.call-cc.org/egg/syslog-port
[chickadee]: http://api.call-cc.org/doc/syslog-port

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
3-Clause BSD. See LICENSE for details.
