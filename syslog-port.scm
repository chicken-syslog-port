;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; syslog-port.scm - Write to syslog as an output port.
;;;
;;; Copyright (c) 2016, Evan Hanson <evhan@foldling.org>.
;;; See LICENSE for details.
;;;

(declare
  (module syslog-port)
  (import data-structures ports syslog)
  (export current-syslog-port open-syslog)
  (disable-interrupts)
  (fixnum-arithmetic))

;;
;; Creates an output port with the given syslog configuration.
;;
;; The resulting port will buffer output until a newline is encountered,
;; at which point its contents will be flushed to the syslog.
;;
(: open-syslog (string fixnum #!optional fixnum fixnum -> output-port))
(define (open-syslog identity priority #!optional (options opt/pid) (facility facility/user))
  (define buffer
    (make-string 0))
  (define (writer s)
    (cond
      ((substring-index "\n" s) =>
       (lambda (i)
         (openlog identity options facility)
         (syslog priority (string-append buffer (substring s 0 i)))
         (set! buffer (make-string 0))
         (writer (substring s (add1 i)))))
      (else
       (set! buffer (string-append buffer s)))))
  (make-output-port writer void))

;;
;; This parameter provides a default syslog port.
;;
;; Its starting value is a port that uses `(program-name)` as its
;; identity and `prio/info` as its message priority.
;;
(: current-syslog-port (#!optional output-port -> *))
(define current-syslog-port
  (make-parameter (open-syslog (program-name) prio/info)))
